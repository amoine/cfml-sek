# Installation

## Automatic

With `opam` installed, just execute `setup.sh`.
It will set-up a switch called `transtack`, install each dependency at
the right version and build the library and its proof.

Please allow at least 15 minutes for the script to run.

To play the files interactively using CoqIDE, run `opam switch
transtack` after `setup.sh` and run the command `opam install -j4
coqide.8.14.1`.

## Semi-automatic

You can install needed dependencies in your current switch by calling
`setup.sh deps`

## Manual

The whole formalization requires, in an opam switch:

* OCaml 4.13 (tested with 4.13.1)
* Coq 8.14 (tested using 8.14.1)
* CFML version 20211215

From the [Coq repository](https://coq.inria.fr/opam/released)
* coq-tlc version 20211215
* coq-cfml-basis and coq-cfml-stdlib version 20211215
