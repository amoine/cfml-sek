#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

if command -v gnutar >/dev/null ; then
  # On MacOS, run "sudo port install gnutar" to obtain gnutar.
  TAR=gnutar
else
  TAR=tar
fi

ARCHIVE=transtack

rm -rf $ARCHIVE $ARCHIVE.tar.gz

mkdir $ARCHIVE

cp -r \
  README.md \
  INSTALL.md \
  setup.sh \
  $ARCHIVE

mkdir $ARCHIVE/src

cp -r \
  src/*.ml \
  src/*.mli \
  src/*.v \
  src/Makefile \
  $ARCHIVE/src

find $ARCHIVE  -type f -name '*.v' -exec sed -i 's/\s*(\*.*TODO.* \*)//g' {} \;

$TAR cvfz $ARCHIVE.tar.gz \
  --exclude-ignore-recursive=.gitignore \
  --owner=0 --group=0 \
  $ARCHIVE

rm -rf $ARCHIVE
