#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

# Automatically answer "yes" to the questions asked by opam.

export OPAMYES=true

# Create a new opam switch. (We assume opam 2 is installed.)

if [[ $# == 0 || $1 != "deps" ]]; then

echo "Creating a new opam switch..."
if opam switch create transtack ocaml-base-compiler.4.13.1
then :
else
echo "Checking if the switch transtack already exists 4.13.1..."
if [ "$(opam exec --switch=transtack ocamlc -- --version)" = 4.13.1 ]
then echo "Fine!"
else
    echo "The switch is not version 4.13.1 of the ocaml compiler; "
    echo "you probably wish to remove it (check first):"
    echo; echo "    opam switch remove transtack"; echo
    echo "and restart the script."
fi
fi
fi

eval "$(opam config env)"

# Update the package list (not a switch-local command, unfortunately).

echo "Updating the list of available packages..."
opam update

# Install Coq & deps.

echo "Installing Coq 8.14.1..."
opam install -j4 coq.8.14.1

echo "Installing TLC & CFML2"
opam repo add coq-released https://coq.inria.fr/opam/released
opam install cfml.20211215 coq-cfml-basis.20211215 coq-cfml-stdlib.20211215 ocamlbuild

echo "Compiling Transtack"
make -j4 -C src

echo -e "\n\n## Compilation complete. ##\n\n"
echo "You can now run \"opam switch transtack\" to get the right environment."
echo "And after, install for example coqide using \"opam install -j4 coqide.8.14.1\"."
echo -e "Next, run \"coqide src/TranstackEphemeral_proof.v\" to start investigating the proof.\n"
