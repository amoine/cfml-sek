type 'a iterator

val of_estack : 'a Transtack.estack -> 'a iterator
val of_pstack : 'a Transtack.pstack -> 'a iterator

val finished : 'a iterator -> bool
val get : 'a iterator -> 'a
val move : 'a iterator -> unit
val set : 'a iterator -> 'a -> unit
