open EChunk

type 'a schunk = {
  support : 'a echunk;
  view : int;
  owner : Id.t option; }

val get_default : 'a schunk -> 'a
val echunk_of_schunk : 'a schunk -> 'a echunk
val schunk_of_echunk : 'a echunk -> Id.t -> 'a schunk

val schunk_empty : 'a -> 'a schunk
val schunk_peek : 'a schunk -> 'a
val schunk_push : 'a schunk -> 'a -> 'a schunk
val schunk_pop : 'a schunk -> 'a schunk * 'a

val schunk_is_empty : 'a schunk -> bool
val schunk_is_full : 'a schunk -> bool
