type t

val create : unit -> t
val eq : t -> t -> bool
val eq_opt : t option -> t -> bool
