type 'a t

val of_list : 'a list -> 'a t

val finished : 'a t -> bool
val get : 'a t -> 'a
val get_and_move : 'a t -> 'a
val move : 'a t -> unit
