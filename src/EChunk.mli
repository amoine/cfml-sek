type 'a echunk = {
  data : 'a array;
  mutable top : int;
  default : 'a;
  }

val capacity : int

val echunk_create : 'a -> 'a echunk
val echunk_pop : 'a echunk -> 'a
val echunk_push : 'a echunk -> 'a -> unit

val echunk_peek : 'a echunk -> 'a

val echunk_is_empty : 'a echunk -> bool
val echunk_is_full : 'a echunk -> bool

val echunk_copy : 'a echunk -> 'a echunk
