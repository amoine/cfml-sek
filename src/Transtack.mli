open EChunk
open SChunk

(* NB: type exposed for Iterator *)
type 'a pstack =
  { pfront : 'a schunk;
    ptail : 'a schunk list; }

type 'a estack =
  { mutable front : 'a echunk;
    mutable tail : 'a schunk list;
    mutable spare : 'a echunk option;
    mutable id : Id.t; }

val ecreate : 'a -> 'a estack
val eis_empty : 'a estack -> bool
val epeek : 'a estack -> 'a
val epush : 'a estack -> 'a -> unit
val epop : 'a estack -> 'a

val pcreate : 'a -> 'a pstack
val pis_empty : 'a pstack -> bool
val ppeek :'a pstack -> 'a
val ppush : 'a pstack -> 'a -> 'a pstack
val ppop : 'a pstack -> 'a pstack * 'a

val pstack_to_estack : 'a pstack -> 'a estack
val estack_to_pstack : 'a estack -> 'a pstack
val copy_with_sharing : 'a estack -> 'a estack
val estack_to_pstack_preserving : 'a estack -> 'a pstack
