# cfml-sek

This repository contains the formalization of a transient stack and
its iterator, a simplification of
[Sek](https://gitlab.inria.fr/fpottier/sek/).

The OCaml library, its specification and its proof can be found in `src/`.
The proof is conducted with Separation Logic extended with time
credits, using [CFML2](https://gitlab.inria.fr/charguer/cfml2).

The whole formalization is described in our CPP'22 paper:
[Specification and Verification of a Transient Stack](https://popl22.sigplan.org/details/CPP-2022-papers/22/Specification-and-Verification-of-a-Transient-Stack).

## Setup

See INSTALL.md

## Organisation

Each OCaml file is associated to one or more specification file.

* `Id.ml`, a module abstracting identifiers, implemented as references to unit.
  * `Id_proof.v`

* `EChunk.ml`, a module for ephemeral chunks.
  * `EChunk_proof.v`

* `SChunk.ml`, a module for shareable chunks, admitting multiple specifications.
  * `SChunk_proof.v` for general proofs about operations on schunks.
  * `SChunkShared_proof.v` for (derived) proofs about operations on shared schunks.
  * `SChunkMaybeOwned_proof.v` for proofs in case ownership is unsure.

* `Transtack.ml`, a module for ephemeral and persistent stacks.
  * `TranstackCommon_proof.v` for common predicates and lemmas.
  * `TranstackEphemeral_proof.v` for proofs on ephemeral stacks.
  * `TranstackEphemeralCopies_proof.v` for proofs on copies of ephemeral stacks.
  * `TranstackPersistent_proof.v` for proofs on persistent stacks.
  * `TranstackConversions_proof.v` for proofs on conversions between ephemeral and persistent stacks.
  * `TranstackEphemeralStandalone_proof.v` for (derived) proofs on stacks without a shared memory.

* `Iterator.ml`, a module for iterators on persistent and ephemeral stacks.
  * `Iterator\_aux_proof.v` for auxiliary lemmas.
  * `Iterator\_proof.v` for actual proof of iterators. We prove a single specification
  for iterators on both ephemeral and persistent stacks.
  * `Iterator\_proof\_derived.v` for derived (and more usable) specifications.

* `Test.ml` :
  * Test_proof for a test proof of our specifications.

There are moreover miscellaneous files:

* `LibSepGroup.v` for iterated separating conjunction on a map.
* `Mono.v` for a notation for monotonicity.
* `Preserve.v` for a pre-order on maps.
* `Memory.v` for the type of memories and its representation predicate.

## Links to Figures

Here is a listing of the figures used in the paper, and where to find
the corresponding Definitions and Lemmas.

* Figure 1. OCaml API of transient stacks:
  `Transtack.mli`
* Figure 3. Internal type definitions for transient stacks:
  `EChunk.ml` `SChunk.ml` `Transtack.ml`
* Figure 5. Type of representation predicates in specifications:
  `Memory.v` `TranstackEphemeral_proof.v` `TranstackPersistent_proof.v`
* Figure 6. Specification of ephemeral stacks:
  `TranstackEphemeral_proof.v`
* Figure 7. Specification of persistent stacks:
  `TranstackPersistent_proof.v`
* Figure 8. Monotonicity lemmas:
  `TranstackEphemeral_proof.v` `TranstackPersistent_proof.v`
* Figure 9. Specification of the conversions:
  `TranstackConversions_proof.v`
* Figure 10. Representation of ephemeral chunks:
  `EChunk_proof.v`
* Figure 11. Representation of the shared memory:
  `Memory.v`
* Figure 12. Representation of shareable chunks:
  `SChunk_proof.v` `SChunkShared.v` `SChunkMaybeOwned_proof.v`
* Figure 13. Representation of persistent stacks:
  `TranstackPersistent_proof.v`
* Figure 14. Representation of ephemeral stacks:
  `TranstackEphemeral_proof.v`
* Figure 15. API of iterators on ephemeral stacks:
  `Iterator.mli`
* Figure 16. Implementation of iterators on ephemeral stacks:
  `Iterator.ml`
* Figure 17. Specification of iterators:
  `Iterator_proof_derived.v`
